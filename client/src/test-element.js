import { LitElement, html, css } from '../node_modules/lit-element/lit-element';

export class TestELement extends LitElement {

    constructor() {
        super();
      }


    static get styles() {
        return css`
          :host {
            display: block;
            padding: 1em;
             border: 1px solid;
              box-shadow: 12px 12px 2px 1px rgba(0, 0, 255, .2);
          }
        `;
      }

    render() {
        return html`
        <div>Hello from testElement!</div>
        `;
    }
}
customElements.define('test-element', TestELement);