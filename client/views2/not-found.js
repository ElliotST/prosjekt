import { LitElement, html, css } from 'lit-element';

class NotFound  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`

      
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


      <nav class="navbar navbar-dark bg-primary navbar-custom">

        <a class="navbar-brand" href="posts">
          <img src="https://i.imgur.com/2x3zCqU.png" style="height:35px;" alt="posts">
        </a>

             <button @click="${this.logout}" class="btn btn-success navbutton">Log out</button>
      </nav>

      <h1>Nothing here</h1>
      <a href="/">Go back</a>
    `;
  }
}

customElements.define('not-found', NotFound);