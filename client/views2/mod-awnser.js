import { LitElement, html, css } from 'lit-element';
import '../views2/componentAwnser';

class ModAwnser  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        allRequests: { type: Array},
        user: {type: Object},
    };
  }

  constructor() {
    super();
    this.authenticateUser();
    this.allRequests = [];
    this.fillRequests();
  }

  render() {
    return html`
    <head>

      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

      <nav class="navbar navbar-dark bg-primary navbar-custom">

          <a class="navbar-brand" href="posts">
            <img src="https://i.imgur.com/2x3zCqU.png" style="height:35px;" alt="posts">
          </a>

          ${ (this.user.userType == "admin")
             ? html`<a href="/modAwnser"> <button class="btn btn-success" type="button">Answer requests</button></a>` : html``}
          ${ (this.user.userType == "user")
             ? html`<a href="/modAsk"> <button class="btn btn-success" type="button">Make requests</button></a>` : html``}
          <button @click="${this.logout}" class="btn btn-success navbutton">Log out</button>

      </nav>

      <div class="container">
        <div class="row">

          <div class="col"></div>
        
          <div class="col-12" style="margin-top: 50px;">
            ${this.allRequests.map(i => html`<comp-awnser-lit-element .oneRequest=${i}></comp-awnser-lit-element>`)}
          </div>

          <div class="col"></div>
  
        </div>
      </div>

    `;
  }

  fillRequests(e) {
    //const data = new FormData(e.target.form);

    fetch(`${window.MyAppGlobals.serverURL}requests`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
    }).then(res => res.json()
    ).then(res => {
      
        this.allRequests = res;
        
    });
}

authenticateUser(){
  console.log("auth function");
  

  fetch(`${window.MyAppGlobals.serverURL}currentUser`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      }
  }).then(res => res.json()
  ).then(res => {
    
    if(res.loggedIn == true){
      this.user = res.user;
      console.log(res.user);
    }else{
      location.href = "/login";
    }
      
  });
}


}

customElements.define('mod-awnser-lit-element', ModAwnser);

 