import { LitElement, html, css } from 'lit-element';
import '../views2/componentComment';
class Comment  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .title {
        background-color: #007bff;
        color: #ffffff;
        border: 0;
        padding-top: 5px;
        padding-bottom: 5px;
        padding-right: 5px;
        padding-left: 5px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
      }
      .content {
        background-color: #ffffff;
        border: 2px outset grey;
        margin-bottom: 10px;
        border-radius: 5px;
      }
      .exitButton {
        padding-top: 0;
        padding-bottom: 0;
        padding-left: 25px;
        padding-right: 25px;
      }
      .exitButton:hover {
        background-color: #e4e4e4;
      }
      .comment {
        background-color: #ffffff;
        border: 1px outset grey;
      }


    `;
  }

  static get properties() {
    return {
      post: {type: Object},
      comments: {type: Array},
      pid : { type: Number },
      user: { type: Object},
      commentsFinnes : {type: Boolean}
    };
  }

  constructor() {
    super();
    this.authenticateUser();
    this.pid = location.search.split('pid=')[1];
    this.findComments()
    this.findPost();
    this.comments = [];
  }

  render() {
    return html`<!doctype html>
    <html lang="en">

      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

      <nav class="navbar navbar-dark bg-primary ">

        <a class="navbar-brand" href="posts">
          <img src="https://i.imgur.com/2x3zCqU.png" style="height:35px;" alt="posts">
        </a>
        ${ (this.user.userType == "admin")
             ? html`<a href="/modAwnser"> <button class="btn btn-success" type="button">Answer requests</button></a>` : html``}
        ${ (this.user.userType == "user")
             ? html`<a href="/modAsk"> <button class="btn btn-success" type="button">Make requests</button></a>` : html``}
        <button @click="${this.logout}" class="btn btn-success">Log out</button>
      </nav>


      <div class="container">
        <div class="row">

          <div class="col"></div>
        
          <div class="col-12" style="margin-top: 50px;">

            <div class="content">
              <div class ="title">
              <h5>USER ${this.post[0].user} : ${this.post[0].title}</h5> 
              </div>
              <p>${this.post[0].content}</p>
            </div>

            <div>
              <label for="comment">Write a comment</label>
              <form onsubmit="javascript: return false;">  
                <textarea id="comment" name="comment" placeholder="Comment.." rows="5" cols="75"></textarea><br>
                <button @click="${this.createComment}" type="submit" value="Submit" class = "btn btn-success">Submit</button>
              </form>
            </div>

            <p style = "font-weight: bold">Comments: </p>
              
            ${this.comments.map(i => html`<component-comment-lit-element .oneComment=${i}></component-comment-lit-element>`)}  

          </div>
          <div class="col"></div>
        </div>
      </div>

    </html>
`;
 }

 createComment(e) {
  const formData = new FormData(e.target.form);

  var object= {pid : this.pid, uid: this.user.id}
  formData.forEach((value, key) => object[key]=value);

  const data = JSON.stringify(object);
  
  fetch(`${window.MyAppGlobals.serverURL}createComment`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      },
      body: data       
  }).then(res => res.json()
  ).then(res => {
      this.post = res;
      window.location.reload();
      
  });
 }



 findPost(e) {

  var obj = { pid : {type: Number}}
  obj.pid = this.pid;
  const data = JSON.stringify(obj);


  fetch(`${window.MyAppGlobals.serverURL}findPost`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      },
      body: data        
  }).then(res => res.json()
  ).then(res => {
      this.post = res;

  });
}



  findComments(e) {

    var obj = { pid : {type: Number}}
    obj.pid = this.pid;
    const data = JSON.stringify(obj);
    console.log(data);
    

    fetch(`${window.MyAppGlobals.serverURL}findComments`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        },
        body: data       
    }).then(res => res.json()
    ).then(res => {
      if(!res.noResults) {
        
        this.comments = res.result;//[0].comment;

      }
      else {
        this.comments.comment = "No comments"

      }
    });
}

authenticateUser(){
  console.log("auth function");
  

  fetch(`${window.MyAppGlobals.serverURL}currentUser`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      }
  }).then(res => res.json()
  ).then(res => {
    
    if(res.loggedIn == true){

      this.user = res.user;
    }else{
      location.href = "/login";
    }
      
  });
}

}

customElements.define('comment-lit-element', Comment);