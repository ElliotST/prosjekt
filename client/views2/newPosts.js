import { LitElement, html, css } from 'lit-element';

export class NewPosts  extends LitElement {

    static get properties() {
        return {
          data: Object,
          user: {type: Object}
        };
      }


  static get styles() {
    return css`
        .container {
            background-color: #ececec;
            border: 1px outset grey;
            border-radius: 5px;
            margin-top: 25px;
        }
        .navbutton {
          background-color: #1e7e34;
          border-color: #1e7e34;
          color: #ffffff; 
        }
        .postbutton {
          background-color: #1e7e34;
          border-color: #1e7e34;
          color: #ffffff;
          margin-bottom: 25px;
        }
    `;
  }

  constructor() {
    super();
    this.authenticateUser();
  }

  render() {
    return html` 

    <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <body>
        
      <nav class="navbar navbar-dark bg-primary ">

        <a class="navbar-brand" href="posts">
          <img src="https://i.imgur.com/2x3zCqU.png" style="height:35px;" alt="posts">
        </a>

        ${ (this.user.userType == "admin")
             ? html`<a href="/modAwnser"> <button class="btn btn-success" type="button">Answer requests</button></a>` : html``}
        ${ (this.user.userType == "user")
             ? html`<a href="/modAsk"> <button class="btn btn-success" type="button">Make requests</button></a>` : html``}
        <button @click="${this.logout}" class="btn btn-success">Log out</button>

        
      </nav>

    </head>

    <body>

    <div class = "container"> 
        <form>
            <label for="title"></label><br>
            <input type="text" id="title" name="title" placeholder="Title.."><br>
            <label for="content"></label><br>
            <textarea id="content" name="content" placeholder="Content.." rows="10" cols="50"></textarea><br><br>
            <button @click="${this.addPost}" type="submit" value="Submit" class = "btn btn-sucsess postbutton">Post</button>
            </form> 
    </div>

    </body>

    `;
  }

  addPost(e) {
    
    const formData = new FormData(e.target.form);

    var object={id: this.user.id}
    formData.forEach((value, key) => object[key]=value);

    const data = JSON.stringify(object);
    //const data = object;
    console.log(data);

    fetch(`${window.MyAppGlobals.serverURL}newPost`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        },
        body: data        //JSON.stringify({a: 7, str: "some string"})
    }).then(res => res.json()
    ).then(res => {
      location.href = "/posts";
    });
    
}

authenticateUser(){
  console.log("auth function");
  

  fetch(`${window.MyAppGlobals.serverURL}currentUser`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      }
  }).then(res => res.json()
  ).then(res => {
    
    if(res.loggedIn == true){
      console.log("Her er jeg auth");
      this.user = res.user;
      console.log(res.user);
    }else{
      location.href = "/login";
    }
      
  });
}


}
customElements.define('new-posts-lit-element', NewPosts);


