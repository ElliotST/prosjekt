import { LitElement, html, css } from 'lit-element';

class EditComment  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .container {
        background-color: #ececec;
        border: 1px outset grey;
        border-radius: 5px;
        margin-top: 25px;
      }
      .title {
        background-color: #c9c9c9;
        border: 1px outset grey;
      }
      .content {
        background-color: #ececec;
        border: 1px outset grey;
      }
      .comment {
        background-color: #ffffff;
        border: 1px outset grey;
      }
      .postbutton {
        background-color: #1e7e34;
        border-color: #1e7e34;
        color: #ffffff;
        margin-bottom: 25px;
      }
    `;
  }

  static get properties() {
    return {
      comment: {type: Object},
      cid : { type: Number },
      user: { type: Object}
    };
  }

  constructor() {
    super();
    this.authenticateUser();
    this.cid = location.search.split('cid=')[1];
    this.findComment();

  }

  render() {
    return html`<!doctype html>
    <html lang="en">
    <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">


    <nav class="navbar navbar-dark bg-primary ">

      <a class="navbar-brand" href="posts">
        <img src="https://i.imgur.com/2x3zCqU.png" style="height:35px;" alt="posts">
      </a>
      ${ (this.user.userType == "admin")
        ? html`<a href="/modAwnser"> <button class="btn btn-success" type="button">Answer requests</button></a>` : html``}
      ${ (this.user.userType == "user")
        ? html`<a href="/modAsk"> <button class="btn btn-success" type="button">Make requests</button></a>` : html``}
      <button @click="${this.logout}" class="btn btn-success">Log out</button>
    </nav>

    <body>

    <div class = "container"> 
      <form>
        <label for="comment"> <b>Comment: </b></label><br>
        <textarea id="comment" name="comment" rows="10" cols="100">${this.comment[0].comment}</textarea><br><br>
        <button @click="${this.editComment}" type="button" value="Submit" class = "btn btn-success postbutton">Submit</button>
      </form> 
    </div>


    </body>




    </html>
`;
 }




 findComment(e) {

    var obj = { cid : {type: Number}}
    obj.cid = this.cid;
    const data = JSON.stringify(obj);
    console.log(data);
  
    fetch(`${window.MyAppGlobals.serverURL}findComment`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        },
        body: data        //JSON.stringify({a: 7, str: "some string"})
    }).then(res => res.json()
    ).then(res => {
        this.comment = res;
    });
  }

  editComment(e) {
    const formData = new FormData(e.target.form);

    var object={cid : this.comment[0].cid, post: this.comment[0].post, user: this.user.id }
    formData.forEach((value, key) => object[key]=value);

    const data = JSON.stringify(object);
    
    fetch(`${window.MyAppGlobals.serverURL}editComment`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        },
        body: data        //JSON.stringify({a: 7, str: "some string"})
    }).then(res => res.json()
    ).then(res => {
        location.href = ("/comment?pid=" + this.comment[0].post)
    });


}


authenticateUser(){
  console.log("auth function");
  
  fetch(`${window.MyAppGlobals.serverURL}currentUser`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      }
  }).then(res => res.json()
  ).then(res => {
    
    if(res.loggedIn == true){
      this.user = res.user;
    }else{
      location.href = "/login";
    }
      
  });
}

}

customElements.define('edit-comment-lit-element', EditComment);