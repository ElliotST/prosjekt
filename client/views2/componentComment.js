import { LitElement, html, css } from 'lit-element';
import '../views2/comments';

export class ComponentComment  extends LitElement {

    static get properties() {
        return {
            
            oneComment: { type: Object},
            user: {type: Object},
            
        }
    }


  static get styles() {
    return css`
      :host {
        display: block;
      }
      .container {
        /* background-color: #ececec; */
        /* border: 1px outset grey; */
      }
      .exitButton {
        padding-top: 0;
        padding-bottom: 0;
        padding-left: 25px;
        padding-right: 25px;
      }
      .exitButton:hover {
        background-color: #e4e4e4;
      }
      .customText {
        color:black;
        text-decoration:none;
      }
      .comment {
        border: 2px outset grey;
        background-color: #ffffff;
        margin-bottom: 10px;
        border-radius: 5px;
      }

    `;
  }

  constructor() {
    super();
    this.authenticateUser();
  }

  render() {
    return html`
    <body>

      <div class = "comment">

        <p>${this.oneComment.comment}</p> 
        
        <div class="btn-group" role="group" aria-label="Basic example">
          ${(this.user.id == this.oneComment.user) ? html`<button @click="${this.editComment}" class = "btn exitButton">Edit</button>` : html``}

          ${((this.user.id == this.oneComment.user) ||
            (this.user.userType == "moderator") ||
            (this.user.userType == "admin")) ? html`<button @click="${this.removeComment}" class = "btn exitButton">Remove</button>` : html``}

          ${((this.user.userType == "moderator") ||
            (this.user.userType == "admin")) ? html`<button @click="${this.blockComment}" class = "btn exitButton">Block</button>` : html``}

        </div>
      </div>
    </body>

    </body>
    `;
  }

  editComment(e) {
    const url = window.MyAppGlobals.rootPath + "editComment?cid=" + this.oneComment.cid;
    window.location.href = url;
  }

  removeComment(e) {

    var obj = { cid : {type: Number}}

    obj.cid = this.oneComment.cid;

    const data = JSON.stringify(obj);

    
    console.log(data);

    fetch(`${window.MyAppGlobals.serverURL}deleteComment`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        },
        body: data        //JSON.stringify({a: 7, str: "some string"})
    }).then(res => res.json()
    ).then(res => {
        window.location.reload();
        
    });
}

blockComment(e) {

  var obj = { cid : {type: Number}}

  obj.cid = this.oneComment.cid;

  const data = JSON.stringify(obj);

  fetch(`${window.MyAppGlobals.serverURL}blockComment`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      },
      body: data        
  }).then(res => res.json()
  ).then(res => {
      window.location.reload();
      
  });
}


  authenticateUser(){
    console.log("auth function");
    
  
    fetch(`${window.MyAppGlobals.serverURL}currentUser`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        }
    }).then(res => res.json()
    ).then(res => {
      
      if(res.loggedIn == true){
        this.user = res.user;
      }else{
        location.href = "/login";
      }
        
    });
  }

}
customElements.define('component-comment-lit-element', ComponentComment);