import { LitElement, html, css } from 'lit-element';

class Frontpage  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
    this.authenticateUser();
  }

  render() {
    return html`
      <h1>Front page, mabye just for testing</h1>
    `;
  }

  authenticateUser(){
    console.log("auth function");
    
  
    fetch(`${window.MyAppGlobals.serverURL}currentUser`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        }
    }).then(res => res.json()
    ).then(res => {
      
      if(res.loggedIn == true){
        location.href = "/posts";
      }else{
        
      }
        
    });
  }

}

customElements.define('frontpage-lit-element', Frontpage);