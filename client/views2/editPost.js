import { LitElement, html, css } from 'lit-element';
import { Posts } from './posts';

class EditPost  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .container {
        background-color: #ececec;
        border: 1px outset grey;
        border-radius: 5px;
        margin-top: 25px;
      }
      .title {
        background-color: #c9c9c9;
        border: 1px outset grey;
      }
      .content {
        background-color: #ececec;
        border: 1px outset grey;
      }
      .comment {
        background-color: #ffffff;
        border: 1px outset grey;
      }
      .postbutton {
        background-color: #1e7e34;
        border-color: #1e7e34;
        color: #ffffff;
        margin-bottom: 25px;
      }
    `;
  }

  static get properties() {
    return {
      post: {type: Object},
      pid : { type: Number },
      user: { type: Object},
    };
  }

  constructor() {
    super();
    this.authenticateUser();
    this.pid = location.search.split('pid=')[1];
    this.findPost();

  }

  render() {
    return html`<!doctype html>
    <html lang="en">
    <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">


    <nav class="navbar navbar-dark bg-primary ">

      <a class="navbar-brand" href="posts">
        <img src="https://i.imgur.com/2x3zCqU.png" style="height:35px;" alt="posts">
      </a>
      ${ (this.user.userType == "admin")
        ? html`<a href="/modAwnser"> <button class="btn btn-success" type="button">Answer requests</button></a>` : html``}
      ${ (this.user.userType == "user")
        ? html`<a href="/modAsk"> <button class="btn btn-success" type="button">Make requests</button></a>` : html``}
      <button @click="${this.logout}" class="btn btn-success">Log out</button>
    </nav>

    <body>

    <div class = "container"> 
      <form>
        <label for="title">Title:</label><br>
        <textarea id="title" name="title" rows="2" cols="100">${this.post[0].title}</textarea><br>
        <label for="content">Content:</label><br>
        <textarea id="content" name="content" rows="10" cols="100">${this.post[0].content}</textarea><br><br>
        <button @click="${this.editPostFunc}" type="button" value="Submit" class = "btn btn-sucsess postbutton">Submit changes</button>
      </form> 
    </div>

    </body>




    </html>
`;
 }


testFunc(e) {
    const formData = new FormData(e.target.form);

    var object={}
    formData.forEach((value, key) => object[key]=value);
    
    const data = JSON.stringify(object);
}

editPostFunc(e) {
    const formData = new FormData(e.target.form);

    var object={}
    formData.forEach((value, key) => object[key]=value);

    this.post[0].title = object.title;
    this.post[0].content = object.content;

    const data = JSON.stringify(this.post[0]);
    console.log(data);
    
    
    fetch(`${window.MyAppGlobals.serverURL}editPost`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        },
        body: data        //JSON.stringify({a: 7, str: "some string"})
    }).then(res => res.json()
    ).then(res => {
      console.log("test worked--------------");
      location.href = "/posts";
        
    });

}



 findPost(e) {

  var obj = { pid : {type: Number}}
  obj.pid = this.pid;
  const data = JSON.stringify(obj);
  console.log(data);

  fetch(`${window.MyAppGlobals.serverURL}findPost`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      },
      body: data        //JSON.stringify({a: 7, str: "some string"})
  }).then(res => res.json()
  ).then(res => {
      this.post = res;
      console.log(this.post)
  });
}

authenticateUser(){
  

  fetch(`${window.MyAppGlobals.serverURL}currentUser`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      }
  }).then(res => res.json()
  ).then(res => {
    
    if(res.loggedIn == true){
      this.user = res.user;
    }else{
      location.href = "/login";
    }
      
  });
}

}

customElements.define('edit-post-lit-element', EditPost);