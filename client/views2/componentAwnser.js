import { LitElement, html, css } from 'lit-element';

class ComponentAWnser  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .container {
        /* background-color: #ececec; */
        /* border: 1px outset grey; */
      }
      .title {
        background-color: #007bff;
        color: #ffffff;
        border: 0;
        padding-top: 5px;
        padding-bottom: 5px;
        padding-right: 5px;
        padding-left: 5px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
      }
      .content {
        background-color: #ffffff;
        border: 2px outset grey;
        margin-bottom: 10px;
        border-radius: 5px;
      }
      .exitButtonGrant {
        padding-top: 0;
        padding-bottom: 0;
        padding-left: 25px;
        padding-right: 25px;
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
      }
      .exitButtonDeny {
        padding-top: 0;
        padding-bottom: 0;
        padding-left: 25px;
        padding-right: 25px;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
      }
      .customText {
        color:black;
        text-decoration:none;
    `;
  }

  static get properties() {
    return {
        oneRequest: { type: Object}
    };
  }

  constructor() {
    super();
    this.authenticateUser();
  }

  render() {
    return html`
    <body>

      <div class="container">
        <div class="content">
          <div class ="title">
            <h5>${this.oneRequest.user}</h5> 
          </div>

          <p>${this.oneRequest.reason}</p>
          <div class="btn-group" role="group" aria-label="Basic example">
            <button @click="${this.grantRequest}" class = "btn-success exitButtonGrant ">Grant request</button>
            <button @click="${this.denyRequest}" class = "btn-danger exitButtonDeny ">Deny request</button>
          </div>
        </div>
      </div>


    </body>
    `;
  }

  grantRequest(){
    
    var obj = { uid : {type: Number}}

    obj.uid = this.oneRequest.user;

    const data = JSON.stringify(obj);

    

    fetch(`${window.MyAppGlobals.serverURL}grantRequest`, { 
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        },
        body: data   
    }).then(res => res.json()
    ).then(res => {
        window.location.reload();
        
        
    });
    
  }


  denyRequest(){

    var obj = { uid : {type: Number}}

    obj.uid = this.oneRequest.user;

    const data = JSON.stringify(obj);


    fetch(`${window.MyAppGlobals.serverURL}denyRequest`, { 
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        },
        body: data        
    }).then(res => res.json()
    ).then(res => {
        window.location.reload();
        
    });
  }

  authenticateUser(){
    console.log("auth function");
    
  
    fetch(`${window.MyAppGlobals.serverURL}currentUser`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        }
    }).then(res => res.json()
    ).then(res => {
      
      if(res.loggedIn == true){
        console.log(res.user);
      }else{
        location.href = "/login";
      }
        
    });
  }

}

customElements.define('comp-awnser-lit-element', ComponentAWnser);