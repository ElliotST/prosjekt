import { LitElement, html, css } from 'lit-element';

class Welcome  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {};
  }

  constructor() {
    location.href = "/login";
    super();
    this.authenticateUser();
  }

  render() {
    return html`
    <!doctype html>
    <html lang="en">
      <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <base href="/">

        <!-- Include the Vaadin.Router library.
       Browsers automatically pick the best of the two options below. -->
       <script nomodule src="https://unpkg.com/@vaadin/router/dist/vaadin-router.umd.js"></script>
      <script type="module" src="https://unpkg.com/@vaadin/router"></script>
    
        <title>Forum</title>

        <style>
            .navbar-custom { 
        background-color: lightgray; 
        } 
    
        </style>
        
      </head>
      <body>

      <nav class="navbar navbar-light navbar-custom">
      <form class="form-inline">
      <h1>Welcome to XYZ, please login to continue.</h1>
      </form>
    </nav>

        
        

        <div class="row justify-content-md-center mt-5"> 
          <div class="jumbotron">
            <h2>New user? Register now!</h2>
            
            <hr class="my-4">
              <a href="/login"> <button class="btn btn-success" type="button">Login</button></a>
                <a href="/register"><button class="btn btn-success" type="button">Register</button></a>
          </div>
      </div>

    
        <!-- Optional JavaScript; choose one of the two! -->
    
        <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    
        <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
        -->


      </body>
    </html>

    `;
  }

  authenticateUser(){
    console.log("auth function");
    
  
    fetch(`${window.MyAppGlobals.serverURL}currentUser`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        }
    }).then(res => res.json()
    ).then(res => {
      
      if(res.loggedIn == true){
        location.href = "/login";
      }else{
        
      }
        
    });
  }
  
}

customElements.define('welcome-lit-element', Welcome);