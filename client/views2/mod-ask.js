import { LitElement, html, css } from 'lit-element';

class ModAsk  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        user: {type: Object}
    };
  }

  constructor() {
    super();
    this.authenticateUser();
  }

  render() {
    return html`
    <!doctype html>
    <html lang="en">
      <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
      </head>
      
    
      <nav class="navbar navbar-dark bg-primary navbar-custom">

        <a class="navbar-brand" href="posts">
          <img src="https://i.imgur.com/2x3zCqU.png" style="height:35px;" alt="posts">
        </a>

        ${ (this.user.userType == "admin")
             ? html`<a href="/modAwnser"> <button class="btn btn-success" type="button">Answer requests</button></a>` : html``}
        ${ (this.user.userType == "user")
             ? html`<a href="/modAsk"> <button class="btn btn-success" type="button">Make requests</button></a>` : html``}
        <button @click="${this.logout}" class="btn btn-success navbutton">Log out</button>
      </nav>

      <body>
        <div class="container"> 
          <div class="row">
            <div class="col"></div>

            <div class="col" style="margin-top: 50px;">
              <form onsubmit="javascript: return false;">
              <label for="request">Why should you become a moderator?</label><br>
              <textarea id="request" name="request" placeholder="Tell us why.." rows="10" cols="50"></textarea><br><br>
              <button @click="${this.sendRequest}" type="submit" value="Submit" class = "btn btn-primary">Send request</button>
            </div>

            <div class="col"></div>
          </div>
        </div>
      </body>
    </html>
    
    `;
  }

  sendRequest(e){

    const formData = new FormData(e.target.form);

    var object={id: this.user.id}
    formData.forEach((value, key) => object[key]=value);
    const data = JSON.stringify(object);
    //const data = object;

    fetch(`${window.MyAppGlobals.serverURL}modAsk`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        },
        body: data       
    }).then(res => res.json()
    ).then(res => {
        location.href = "/posts";
    });
    


  }

  authenticateUser(){
    console.log("auth function");
    
  
    fetch(`${window.MyAppGlobals.serverURL}currentUser`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        }
    }).then(res => res.json()
    ).then(res => {
      
      if(res.loggedIn == true){
        this.user = res.user;
        console.log(this.user.id);
      }else{
        location.href = "/login";
      }
        
    });
  }

}

customElements.define('mod-ask-lit-element', ModAsk);