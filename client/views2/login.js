
import {LitElement, html, css } from 'lit-element';



class Login  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .btn-login {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
      }
      .btn-register {
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
      }
    `;
  }

  static get properties() {
    return {
      users: {type: Array},
      validUser: {type: Boolean},
      user: {type: Object}
      
    };
  }

  constructor() {
    super();
    this.authenticateUser();
    
  }

  render() {
    return html`
    
      <head>

      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  
        <title>Login</title>
           
      </head>
      <body>

        <nav class="navbar navbar-dark bg-primary navbar-custom">

          <a class="navbar-brand" href="posts">
            <img src="https://i.imgur.com/2x3zCqU.png" style="height:35px;" alt="posts">
          </a>


          <form class="form-inline">
              <a href="/login"> <button class=" btn btn-login btn-success active" type="button">Login</button></a>
             <a href="/register"><button class=" btn btn-register btn-success" type="button">Register</button></a>
          </form>


        
        </nav>


    
        <div class="row justify-content-md-center mt-5"> 
            <div class="col-3">
                <form onsubmit="javascript: return false;">
                  <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email">
                  </div>
                  <div class="form-group">
                   <label for="password">Password</label>
                      <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <button type="submit" @click="${this.login}" class="btn btn-primary">Login</button>
                 </form>

            
                 
          </div>
        </div>
      </body>
    </html>
    `;
  }

  

login(e) {
    
  const formData = new FormData(e.target.form);

  var object={}
  formData.forEach((value, key) => object[key]=value);

  const data = JSON.stringify(object);

  fetch(`${window.MyAppGlobals.serverURL}login`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      },
      body: data
  }).then(res => res.json()
  ).then(res => {
    if(res.valid){ 
    location.href = "/posts";
    }else{
      console.log(res.message);
    }
      
  });
}

authenticateUser(){
  console.log("auth function");
  

  fetch(`${window.MyAppGlobals.serverURL}currentUser`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      }
  }).then(res => res.json()
  ).then(res => {
    
    if(res.loggedIn == true){
      location.href = "/posts";
    }else{
      
    }
      
  });
}

  
}

customElements.define('login-lit-element', Login);

