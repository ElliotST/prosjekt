
import {Router} from '@vaadin/router';

import './login';
import './register';
import './posts';
import './welcome';
import './not-found';
import './frontpage'
import './newPosts';
import './comments';
import './editPost';
import './mod-ask';
import './mod-awnser';
import './editComment';

window.addEventListener('load', () => { 
  initRouter();
});

function initRouter() {
  const router = new Router(document.querySelector('main')); 
  router.setRoutes([
    {
      path: '/',
      component: 'welcome-lit-element'
    },
    {
      path: '/login',
      component: 'login-lit-element',
      
    },
    {
      path: '/register',
      component: 'register-lit-element',
      
    },
    {
      path: '/posts',
      component: "posts-lit-element",
    },
    {

      path: '/frontpage',
      component: "frontpage-lit-element",
    },
    { 
      path: '/newPosts',
      component: 'new-posts-lit-element',
      

    },
    {
      path: '/comment',
      component: 'comment-lit-element',

    },
    {
      path: '/editPost',
      component: 'edit-post-lit-element',
    },
    {
      path: '/modAsk',
      component: 'mod-ask-lit-element',
    },
    {
      path: '/modAwnser',
      component: 'mod-awnser-lit-element',
    },
    {
      path: '/editComment',
      component: 'edit-comment-lit-element',

    },
    {
      path: '(.*)',
      component: 'not-found',

    }
  ]);
}


