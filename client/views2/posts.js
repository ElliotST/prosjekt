import { LitElement, html, css } from 'lit-element';
import '../views2/componentPost';

export class Posts  extends LitElement {

    static get properties() {
        return {
            userName: {type: String},
            allPosts: { type: Array},
            content: {type: String},
            date: { type : String},
            topic: { type: String },
            numPosts: { type: Number },


            user: {type: Object},


            allComments: { type: Array },
            currentPostID: { type: Array}

            //posts1 : { type: Object}
        };
      }


  static get styles() {
    return css`
      :host {
        display: block;
      }
      .container {
        /* background-color: #ececec; */
        /* border: 1px outset grey; */
      }
      .title {
        background-color: #c9c9c9;
        border: 1px outset grey;
      }
      .content {
        background-color: #ececec;
        border: 1px outset grey;
      }
      .exitButton {
        margin-left: 98%;
      }
      .navbutton {
        background-color: #1e7e34;
        border-color: #1e7e34;
      }
    `;
  }

  constructor() {
    super();
    this.authenticateUser();
    

    this.allPosts = []; //this.numPosts

    this.searchFor = location.search.split('searchFor=')[1];

    if(!this.searchFor) {
      this.searchFor = [];
      console.log("went into")
    }

    console.log("this.searchFor: " +this.searchFor);
    this.search();
    
  }

  render() {
    return html`


    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <body>
        
      <nav class="navbar navbar-dark bg-primary navbar-custom">

        <a class="navbar-brand" href="posts">
          <img src="https://i.imgur.com/2x3zCqU.png" style="height:35px;" alt="posts">
        </a>

        ${ (this.user.userType == "admin")
          ? html`<a href="/modAwnser"> <button class="btn btn-success" type="button">Answer requests</button></a>` : html``}
        ${ (this.user.userType == "user")
          ? html`<a href="/modAsk"> <button class="btn btn-success" type="button">Make requests</button></a>` : html``}
        <button @click="${this.logout}" class="btn btn-success navbutton">Log out</button>

        <form class="form-inline">
          <input name="searchFor" id="searchFor" class="form-control mr-sm-2" type="search" placeholder="Search for title" aria-label="Search">
          <button @click="${this.openSearch}" class="btn btn-dark" type="submit">Search</button>
        </form>

      </nav>

      <div class="container">
        <div class="row">

          <div class="col"></div>
        
          <div class="col-12" style="margin-top: 50px;">   
            
              <a href="/newPosts"> <button class="btn btn-success active btn-lg btn-block" style="margin-bottom: 10px;" type="button">New Post</button></a>
            </form>

            ${this.allPosts.map(i => html`<component-posts-lit-element .onePost=${i}></component-posts-lit-element>`)} 
        
          </div>

          <div class="col"></div>

        </div>
      </div>

    </body>
      
    `;
  }

  


//this.openSearch();


search(e) {

  var obj = { title : {type: String}}
  obj.title = this.searchFor;

  const data = JSON.stringify(obj);


  fetch(`${window.MyAppGlobals.serverURL}search`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      },
      body: data
  }).then(res => res.json()
  ).then(res => {
    this.allPosts = res;
  });
}



openSearch(e){
  const url = window.MyAppGlobals.rootPath + "posts?searchFor=" + this.searchFor;
  window.location.href = url;
}

  fillPosts(e) {
    //const data = new FormData(e.target.form);

    fetch(`${window.MyAppGlobals.serverURL}posts`, { // Bytt dette med din path
        method: 'GET',
        credentials: "include",
        //body: data
    }).then(res => res.json()
    ).then(res => {
      // for(let i = 0; i < this.numPosts; i++) {
        this.allPosts = res;

        
    });
}


authenticateUser(){
  

  fetch(`${window.MyAppGlobals.serverURL}currentUser`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      }
  }).then(res => res.json()
  ).then(res => {
    
    if(res.loggedIn == true){
      this.user = res.user;
    }else{
      location.href = "/login";
    }
      
  });
}

logout(){
  console.log("logout function");
  

  fetch(`${window.MyAppGlobals.serverURL}logout`, { // Bytt dette med din path
      method: 'DELETE',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      }
  }).then(res => res.json()
  ).then(res => {
    
      location.href = "/login";
      
  });
}


}
customElements.define('posts-lit-element', Posts);


