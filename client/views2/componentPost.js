import { LitElement, html, css } from 'lit-element';
import '../views2/comments';

export class ComponentPost  extends LitElement {

    static get properties() {
        return {
            onePost: { type: Object},
            user: {type: Object},
            haveLiked: {type: Boolean},
            numLikes: {type: Number},
            name: {type: String}
            

        }
    }


  static get styles() {
    return css`
      :host {
        display: block;
      }
      .container {
        /* background-color: #ececec; */
        /* border: 1px outset grey; */
      }
      .title {
        background-color: #007bff;
        color: #ffffff;
        border: 0;
        padding-top: 5px;
        padding-bottom: 5px;
        padding-right: 5px;
        padding-left: 5px;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
      }
      .content {
        background-color: #ffffff;
        border: 2px outset grey;
        margin-bottom: 10px;
        border-radius: 5px;
      }
      .exitButton {
        padding-top: 0;
        padding-bottom: 0;
        padding-left: 25px;
        padding-right: 25px;
        
      }
      .customText {
        color:black;
        text-decoration:none;
      }

    `;
  }

  constructor() {
    super();
    this.authenticateUser();
    
  
  }

  render() {
    return html`
    <body>

    ${this.postsLiked()}
    ${this.countLikes()}

      
        <div class="content">
        <div @click=${this.openPost}>
          <div class ="title">
            <h5>${this.onePost.title} | posted by: ${this.onePost.user}</h5> 
          </div>

          <p>${this.onePost.content}</p>
          </div>
          <div class="btn-group" role="group" aria-label="Basic example">
            ${((this.user.id == this.onePost.user) ||
              (this.user.userType == "moderator") ||
              (this.user.userType == "admin")) ? html`<button @click="${this.removePost}" class = "btn exitButton">Remove</button>` : html``}
    
            ${((this.user.id == this.onePost.user) ||
              (this.user.userType == "moderator") ||
              (this.user.userType == "admin")) ? html`<button @click="${this.blockPost}" class = "btn exitButton">Block</button>` : html``}
    
            ${((this.user.id == this.onePost.user) ||
              (this.user.userType == "moderator") ||
              (this.user.userType == "admin")) ? html`<button @click="${this.editPost}" class = "btn exitButton">Edit</button>` : html``}


              ${(!this.haveLiked) ?
                html` 
                  <button @click="${this.likePost}" class = "btn exitButton">Like</button>`: 
                html`
                  <button @click="${this.unLikePost}" class = "btn btn-success exitButton">Like</button>`
              }
              <button>${this.numLikes}</button>



          </div>
        </div>
    </body>

    `;
  }

  //<button @click="${this.likePost}" class = "btn exitButton">Like</button>

  countLikes(e) {
    console.log("in postsLikes function");

    //const data = JSON.stringify(this.onePost.pid);
    //const data = object;
    //console.log(data);

    var obj = { post : this.onePost.pid}

    const data = JSON.stringify(obj);

    console.log(data);

    fetch(`${window.MyAppGlobals.serverURL}countLikes`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        },
        body: data        //JSON.stringify({a: 7, str: "some string"})
    }).then(res => res.json()
    ).then(res => {
      this.numLikes = res.number;
    });
    console.log("test worked--------------");
  }

  postsLiked(e) {
    console.log("in postsLikes function");

    //const data = JSON.stringify(this.onePost.pid);
    //const data = object;
    //console.log(data);

    var obj = { post : this.onePost.pid, user: this.user.id}

    const data = JSON.stringify(obj);

    console.log(data);

    fetch(`${window.MyAppGlobals.serverURL}postsLiked`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        },
        body: data        //JSON.stringify({a: 7, str: "some string"})
    }).then(res => res.json()
    ).then(res => {
        console.log("'CHECK IF IT'S LIKED OR NOT!");
        console.log("CHECK THIS LINE : " + res.alreadyLiked)
        this.haveLiked = res.alreadyLiked;
    });
    console.log("test worked--------------");
  }

  unLikePost(e) {
    console.log("in unLike function");

    //const data = JSON.stringify(this.onePost.pid);
    //const data = object;
    //console.log(data);

    var obj = { post : this.onePost.pid, user: this.user.id}

    const data = JSON.stringify(obj);

    console.log(data);
    
    fetch(`${window.MyAppGlobals.serverURL}removeLike`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        },
        body: data        //JSON.stringify({a: 7, str: "some string"})
    }).then(res => res.json()
    ).then(res => {
        console.log("you should refresh to see if it worked!")
        //this.allPosts = res;
        window.location.reload();
        
    });
    console.log("test worked--------------");
  }



  likePost(e) {
    

    console.log("in likePost function");

    //const data = JSON.stringify(this.onePost.pid);
    //const data = object;
    //console.log(data);

    var obj = { post : this.onePost.pid, user: this.user.id}

    const data = JSON.stringify(obj);

    console.log(data);
    
    fetch(`${window.MyAppGlobals.serverURL}addLike`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        },
        body: data        //JSON.stringify({a: 7, str: "some string"})
    }).then(res => res.json()
    ).then(res => {
        console.log("you should refresh to see if it worked!")
        //this.allPosts = res;
        window.location.reload();
        
    });
    console.log("test worked--------------");



  }

  openPost(e) {
    const url = window.MyAppGlobals.rootPath + "comment?pid=" + this.onePost.pid;
    window.location.href = url;
    
  }

  editPost(e) {
    const url = window.MyAppGlobals.rootPath + "editPost?pid=" + this.onePost.pid;
    window.location.href = url;
  }

  removePost(e) {

    var obj = { pid : {type: Number}}

    obj.pid = this.onePost.pid;

    const data = JSON.stringify(obj);

    
    console.log(data);

    fetch(`${window.MyAppGlobals.serverURL}deletePost`, { // Bytt dette med din path
        method: 'POST',
        credentials: "include",
        headers: {
          'Content-Type' : 'application/json'
        },
        body: data        //JSON.stringify({a: 7, str: "some string"})
    }).then(res => res.json()
    ).then(res => {
        window.location.reload();
        
    });
}

blockPost(e) {

  var obj = { pid : {type: Number}}

  obj.pid = this.onePost.pid;

  const data = JSON.stringify(obj);

  
  console.log(data);

  fetch(`${window.MyAppGlobals.serverURL}blockPost`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      },
      body: data        //JSON.stringify({a: 7, str: "some string"})
  }).then(res => res.json()
  ).then(res => {
      window.location.reload();
      
      
  });
}


authenticateUser(){
  console.log("auth function");
  

  fetch(`${window.MyAppGlobals.serverURL}currentUser`, { // Bytt dette med din path
      method: 'POST',
      credentials: "include",
      headers: {
        'Content-Type' : 'application/json'
      }
  }).then(res => res.json()
  ).then(res => {
    
    if(res.loggedIn == true){
      this.user = res.user;
      console.log(res.user);
    }else{
      location.href = "/login";
    }
      
  });
}

}
customElements.define('component-posts-lit-element', ComponentPost);