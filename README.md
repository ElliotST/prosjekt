# Prosjekt - PROG2053

## Users in database: 

## Admin:
- email: guy@gmail.com
- password: guy23!

## Moderator: 
- email: panda@gmail.com
- password: ilovepandas

## Users:
- email: lisa@gmail.com
- password: lisa15cool

- email: iron@gmail.com
- password: batman

## Group members:     
- Elliot Sveum Torp
- Rihards Daniels Ustinovics
- Sindre Emil Vikre
   
## Setup: 
- docker-compose up -d   
- npm i in client and server folder.

The project runs on localhost:8080   