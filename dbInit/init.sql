/* Create database */
CREATE DATABASE IF NOT EXISTS `prog2053-proj`;

/* Create tables */
CREATE TABLE `users` (
  `uid` BIGINT(8) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(128) NOT NULL,
  `email` VARCHAR(128) NOT NULL,
  `password` VARCHAR(128) NOT NULL,
  `userType` ENUM('admin', 'moderator', 'user') DEFAULT "user" NOT NULL,
  `picture` LONGBLOB DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `posts` (
`pid` BIGINT(8) NOT NULL AUTO_INCREMENT,
`user` BIGINT(8) NOT NULL,
`title` VARCHAR(200) NOT NULL, 
`content` VARCHAR(20000) NOT NULL,
`blockedP` tinyint(1) DEFAULT 0,
PRIMARY KEY (`pid`),
FOREIGN KEY (`user`) REFERENCES users(`uid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `comments` (
`cid` BIGINT(8) NOT NULL AUTO_INCREMENT,
`post` BIGINT(8) NOT NULL,
`user` BIGINT(8) NOT NULL,
`comment` VARCHAR(20000),
`blockedC` tinyint(1) DEFAULT 0,
PRIMARY KEY (`cid`),
FOREIGN KEY (`user`) REFERENCES users(`uid`),
FOREIGN KEY(`post`) REFERENCES posts(`pid`) ON DELETE CASCADE
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `postLikes` (
 `post` bigint(8) NOT NULL,
 `user` bigint(8) NOT NULL,
 `like` tinyint(1) DEFAULT NULL,
 KEY `user` (`user`),
 KEY `post` (`post`),
 CONSTRAINT `postLikes_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`uid`) ON DELETE CASCADE,
 CONSTRAINT `postLikes_ibfk_2` FOREIGN KEY (`post`) REFERENCES `posts` (`pid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `modRequests` (
`user` BIGINT(8) NOT NULL,
`reason` VARCHAR(20000),
FOREIGN KEY (`user`) REFERENCES users(`uid`) ON DELETE CASCADE
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

/* Insert data */
INSERT INTO `users` (`uid`, `username`,`email`, `password`, `userType`) VALUES ('1','FunGuy','guy@gmail.com','$2a$10$xA3IOHTGdwTNPvUr88Wi/.sql2jRFiuFKrrkoI2vgNAZPtkEU7Ug6','admin'),
('2', 'PandaLover','panda@gmail.com','$2a$10$jU/bAh2l8SeHiMIIAQ3IYuEvo3C5N3SZDwm2pcbGOZHWdk3bUxn/i','moderator'),
('3','lisacool','lisa@gmail.com','$2a$10$VKpqAuwnqH/VM14fSXio7e1s0xrsAc6MmDOpuKpnlNOX/LmzwZgqC','user'),
('4','ironman','iron@gmail.com','$2a$10$pN8QnxAeTJjbnKCdEk3PquhQY3k7ZMzjlr43dXJQ0qpysBHOZOUWm','user');

INSERT INTO `posts` (`pid`, `user`,`title`, `content`) VALUES ('1','1','Airplanes','Airplanes are very cool, dont you think?'),
('2','3','Cows','I like cows, they are pretty.'),
('3','3','Travel','What is the nicest place to travel?..Paris?'),
('4','4','Hulk','Sometimes I wonder what it is like to be the hulk. '),
('5','1','Hello!','Hello. This is the captain speaking.');

INSERT INTO `comments` (`cid`, `post`,`user`, `comment`) VALUES ('1','1','2','Yes a agree! Airplanes are cool!'),
('2','3','3','I can travel anywhere!'),
('3','5','1','Yes. I am Groot...');

INSERT INTO `modRequests` (`user`, `reason`) VALUES ('3', 'I am a very good person!');

INSERT INTO `postLikes` (`post`, `user`, `like`) VALUES ('1', '1', '1');