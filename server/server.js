import dotenv from 'dotenv';
if(process.env.NODE_ENV !== 'production'){
  dotenv.config();
}

//"use strict";

import path from 'path';
import mysql from 'mysql';
import bcrypt from 'bcryptjs';
import passport from 'passport';
import initialize from './passport-config.js';
import passportLocal from 'passport-local';
import express from 'express';
import flash from 'express-flash';
import session from 'express-session';
import methodOverride from 'method-override';
import { callbackify } from 'util';
import bodyParser from 'body-parser';

//var jsonParser = bodyParser.json();

//var urlencodedParser = bodyParser.urlencoded({ extended: false })

const app = express();
const PORT = 8081;

app.listen(PORT, () => {
  console.log('Running...');
})
app.use(express.static(path.resolve() + '/server'));
app.use(bodyParser.urlencoded({extended : true }));
app.use(bodyParser.json());
// Add headers
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});


app.use(express.urlencoded({ extended: false}))  //login ?
app.use(flash());                                //login ? 
app.use(session({                               //login husk .env
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false
}));

app.use(passport.initialize());     //login
app.use(passport.session());        //login
app.use(methodOverride('_method'));  //login ?

initialize(  
  passport, 
  email => users.find(user => user.email === email),
  id => users.find(user => user.id === id)
);


const users = [];

var db = mysql.createConnection({
  host: "db",
  user: "admin",
  password: "password",
  database: 'prog2053-proj'
});

db.connect(function (err) {
  if (err) {
    throw err;
  }
  console.log("Connected!");
});
 

app.post('/login', (req, res) => {
  

   let sql = 'SELECT * FROM users WHERE email = "'+req.body.email+'" ';
   db.query(sql, async function (err, results) {

       if (err) {

         res.send({valid: false})

       } else if(Object.keys(results).length === 0){ 
        res.send({valid: false})

       }else {

var testCurrentUID;

        
          if(await bcrypt.compare(req.body.password, results[0].password) && 
                                  req.body.email == results[0].email){
              users.push({
               id: results[0].uid,
               email: results[0].email,
               password: results[0].password,
               username: results[0].username,
               userType: results[0].userType
         });

         
              passport.authenticate("local")(req, res, function() {
                if(req.isAuthenticated()){
                  console.log(req.user.username);
                  res.send({valid: true});
                }
              })

          }else{
            res.send({valid: false, message: "User was not logged in"});
          }

        
       }
      });

});

app.delete('/logout', (req, res) => {
  req.logOut();
  res.send({logout: true});
})

app.post('/currentUser', function(req, res) {
 
  if (req.isAuthenticated()) {
      res.send({loggedIn: true, user: req.user});
  } else {
    console.log("User is not logged in")
    res.send({loggedIn: false});
  }
});

app.post('/register', async (req, res) => {
  try{
     const hashedPassword = await bcrypt.hash(req.body.password, 10)

     let sql = 'SELECT * FROM users WHERE email = "'+req.body.email+'" ';
   db.query(sql, async function (err, results) {

    if(Object.keys(results).length === 0){ 

      let sql = 'INSERT INTO users (email, password, username) VALUES ("'+req.body.email+'", "'+hashedPassword+'", "'+req.body.username+'")';
      db.query(sql, (err, results2) => {
        if(err) throw err;
       
      });
      res.send({registerd: true});
    }else{
      res.send({registerd: false, message: "Email is already registerd"});
    }
    });
    
  }catch{
    res.send({registerd: false, message: "User was not registerd"});
  }
})


app.get('/getUsers', function (req, res) {
  db.query('SELECT * FROM users', function (err, result) {
    if (err) {
      res.status(400).send('Error in database operation.');
    } else {
      res.send(result);
    }
  });
});

app.get('/countposts', function (req, res) {

  db.query('SELECT COUNT(pid) as numPosts FROM posts', (err, result) => {
    if(err) {
      res.status(401).send('Error in count posts');
    }
    else {
        res.send(result);
    }
  })
})

app.post('/deletePost', (req, res) => {

  console.log(req.body.pid)
  let sql = 'DELETE FROM posts WHERE pid = "'+req.body.pid+'"'
  db.query(sql, (err, results) => {
      if(err) throw err;
      console.log("RESULTATER   :   " + results)
      res.send(results)
  }) 
})

app.post('/modAsk', (req, res) => {
  let sql = 'INSERT INTO modRequests (user, reason) VALUES ("'+req.body.id+'", "'+req.body.request+'")';
  db.query(sql, (err, results) => {
      if(err) throw err;
      console.log("request query");
      res.send({message: "request was sendt"});
  }) 
})

app.post('/requests', function (req, res) {
  
  db.query('SELECT * FROM modRequests', (err,result) => {
    
        if (err) {
          res.status(400).send('Error in database operation.');
        } else {
          res.send(result);
        }
      }
    )
    
  });


app.get('/posts', function (req, res) {
  
  db.query('SELECT * FROM posts WHERE blockedP = 0', (err,result) => {
    if(err) {console.log("error-get/posts")}
    
        if (err) {
          res.status(400).send('Error in database operation.');
        } else {
          res.send(result);
        }
      }
    )
    
  });



app.post('/newPost', (req, res) => {

    let sql = 'INSERT INTO posts (user, title, content) VALUES ("'+req.body.id+'", "'+req.body.title+'", "'+req.body.content+'")';
    db.query(sql, (err, results) => {
        if(err) throw err;
        console.log(results)
        res.send(true)
    }) 

    
})


app.post('/createComment', (req, res) => {

  let sql = 'INSERT INTO comments (post, user, comment) VALUES ("'+req.body.pid+'", "'+req.body.uid+'", "'+req.body.comment+'")';
  db.query(sql, (err, results) => {
      if(err) throw err;
      res.send(true)
  }) 
})



app.post('/deletePost', (req, res) => {

  let sql = 'DELETE FROM posts WHERE pid = "'+req.body.pid+'"'
  db.query(sql, (err, results) => {
      if(err) throw err;
      res.send(results)
  }) 
})

app.post('/blockPost', (req, res) => {

  console.log(req.body.pid)
  let sql = 'UPDATE posts SET blockedP = 1 WHERE pid = "'+req.body.pid+'"'
  db.query(sql, (err, results) => {
      if(err) throw err;
      res.send(results)
  }) 
})

app.post('/deleteComment', (req, res) => {

  console.log(req.body.cid)
  let sql = 'DELETE FROM comments WHERE cid = "'+req.body.cid+'"'
  db.query(sql, (err, results) => {
      if(err) throw err;
      res.send(results)
  }) 
})

app.post('/blockComment', (req, res) => {

  console.log(req.body.pid)
  let sql = 'UPDATE comments SET blockedC = 1 WHERE cid = "'+req.body.cid+'"'
  db.query(sql, (err, results) => {
      if(err) throw err;
      res.send(results)
  }) 
})

app.post('/findComments', function (req, res) {
  
  db.query('SELECT * FROM comments WHERE post = "'+req.body.pid+'" AND blockedC = 0', (err,result) => {
    if(err) {console.log("error-post/findComments")}

        if (err) {
          res.status(400).send('Error in database operation.');
        } else {

          if(Object.keys(result).length === 0){
            res.send({noResults: true} )
          }
          else {
            res.send({noResults: false, result});
          }
        }
      }
    )
    
});



app.post('/findComment', function (req, res) {
    
    db.query('SELECT * FROM comments WHERE cid = "'+req.body.cid+'" AND blockedC = 0', (err,result) => {
      if(err) {console.log("error-post/findComments")}
  
          if (err) {
            res.status(400).send('Error in database operation.');
          } else {
              res.send(result);
            }
          }
)});
  


app.post('/findPost', function (req, res) {
    //console.log(req.user.username);
    
    db.query('SELECT * FROM posts WHERE pid = "'+req.body.pid+'"', (err,result) => {
      if(err) {console.log("error-post/findComments")}
  
          if (err) {
            res.status(400).send('Error in database operation.');
          } else {
            res.send(result);
          }
        }
      )
      
});



app.post('/editPost', function (req, res) {

  db.query('UPDATE posts SET title = "'+req.body.title+'", content = "'+req.body.content+'" WHERE pid = "'+req.body.pid+'"', (err,result) => {
    if(err) {console.log("error-post/editPost")}

        if (err) {
          res.status(400).send('Error in database operation.');
        } else {
          res.send(result);
        }
      }
    )
    
});



app.post('/editComment', function (req, res) {

  db.query('UPDATE comments SET post = "'+req.body.post+'", user = "'+req.body.user+'", comment = "'+req.body.comment+'" WHERE cid = "'+req.body.cid+'"', (err,result) => {
    if(err) {console.log("error-post/editComment")}

        if (err) {
          res.status(400).send('Error in database operation.');
        } else {
          res.send(result);
        }
      }
    )
    
});



app.post('/grantRequest', (req, res) => {

  let sql = 'DELETE FROM modRequests WHERE user = "'+req.body.uid+'"';
  let sql2 = 'UPDATE users SET userType = "moderator" WHERE uid = "'+req.body.uid+'"'
  db.query(sql, (err, results) => {
      if(err) throw err;

      
      db.query(sql2, (err, results) => {
        if(err) throw err;
    }) 

      res.send({granted: true});
  }) 
})


app.post('/denyRequest', (req, res) => {

  let sql = 'DELETE FROM modRequests WHERE user = "'+req.body.uid+'"';
  db.query(sql, (err, results) => {
      if(err) throw err;
      res.send({granted: false})
  }) 
})


app.post('/search', function (req, res) {

  let sql = 'SELECT * FROM posts WHERE title LIKE "%'+req.body.title+'%"  ' ;

  db.query(sql, (err, result) => {
    if(err) {
      console.log("err-app-post/search");
    }
    else {
        res.send(result);
    }
  })
})
  

app.get('/commentID', function (req, res) {

  let sql = 'SELECT MAX(cid) AS maxCID FROM comments'
  db.query(sql, (err, result1) => {
    if(err) {
      res.status(401).send('Error in count posts');
    }
    else {
        res.send(result1);
    }
  })
})


app.post('/postsLiked', (req, res) => {

console.log("req.body.uid : " + req.body.user)
console.log("req.body.post : " + req.body.post)

  let sql = 'SELECT * FROM postLikes WHERE user = "'+req.body.user+'" AND post = "'+req.body.post+'"';
  db.query(sql, (err, results) => {

      if(err) throw err;

      if(Object.keys(results).length === 0){
        res.send({alreadyLiked: false})
      }
      else {
        //res.send({alreadyLiked: false});
      res.send({alreadyLiked: true});
      }
  }) 
})

app.post('/addLike', (req, res) => {

  console.log("req.body.uid : " + req.body.user)
  console.log("req.body.post : " + req.body.post)

    let sql = 'INSERT INTO `postLikes` (`post`, `user`, `like`) VALUES ("'+req.body.post+'", "'+req.body.user+'", "1")';

    //INSERT INTO `postLikes` (`post`, `user`, `like`) VALUES ("2", "6", 1)

    db.query(sql, (err, results) => {
  
        if(err) throw err;
        else {
          res.send(results);
        }
    }) 
  })

  app.post('/removeLike', (req, res) => {

    console.log("req.body.uid : " + req.body.user)
    console.log("req.body.post : " + req.body.post)
  
      let sql = 'DELETE FROM postLikes WHERE user = "'+req.body.user+'" AND post = "'+req.body.post+'"';
      db.query(sql, (err, results) => {
    
          if(err) throw err;
          else {
            res.send(results);
          }
      }) 
     })


app.post('/countLikes', (req, res) => {
    
  let sql = 'SELECT COUNT(*) as numberOfPosts FROM postLikes WHERE post = "'+req.body.post+'"';
  db.query(sql, (err, results) => {
      
      if(err) throw err;
      else {
        //console.log("THIS IS WHERE I SHOULD CHECK: " + results)
        //console.log("THIS IS WHERE I SHOULD CHECK: " + results.numberOfPosts)
        console.log("THIS IS WHERE I SHOULD CHECK: " + results[0].numberOfPosts)
        res.send({number : results[0].numberOfPosts});
      }
  }) 
})

     





